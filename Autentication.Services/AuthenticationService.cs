﻿using Autentication.Domain.Interfaces.Repository;
using Autentication.Domain.Interfaces.Service;
using Autentication.Domain.Shared;

namespace Autentication.Services
{
    public class AuthenticationService : IAuthenticationService
    {
        private readonly IUsuarioRepository _usuarioRepository;
        public AuthenticationService(IUsuarioRepository usuarioRepository)
        {
            _usuarioRepository = usuarioRepository;
        }
        public async Task<string> Login(string email, string senha)
        {
            var result = await _usuarioRepository.GetByEmail(email);

            var _senhaCriptografada = Cryptography.Encrypt(senha);

            if (result != null && result.Senha == _senhaCriptografada)
            {
                return Token.GenerateToken(result);

            }

            return string.Empty;
        }
    }
}
