﻿using Autentication.Domain.Contracts.Endereco;
using Autentication.Domain.Entities;
using Autentication.Domain.Interfaces;
using Autentication.Domain.Interfaces.Repository;
using Autentication.Domain.Interfaces.Service;
using AutoMapper;

namespace Autentication.Services
{
    public class EnderecoService : IEnderecoService
    {
        public readonly IEnderecoRepository _enderecoRepository;
        public readonly IMapper _mapper;
        public EnderecoService(IEnderecoRepository enderecoRepository, IMapper mapper)
        {
            _enderecoRepository = enderecoRepository;
            _mapper = mapper;
        }
        public async Task<IEnumerable<EnderecoResponse>> Get()
        {
            var ListaEnderecoRetornoBaseDados = await _enderecoRepository.Get();

            return _mapper.Map<IEnumerable<EnderecoResponse>>(ListaEnderecoRetornoBaseDados);
        }
   
        public async Task<EnderecoResponse> GetById(int id)
        {
            var enderecoRetornoBaseDados = await _enderecoRepository.GetById(id);

            return _mapper.Map<EnderecoResponse>(enderecoRetornoBaseDados);
        }

        public async Task<EnderecoResponse> Post(EnderecoRequest enderecoRequest)
        {
            var requestEnderecoEntities = _mapper.Map<EnderecoEntities>(enderecoRequest);

            var enderecoCadastrado = await _enderecoRepository.Post(requestEnderecoEntities);

            return _mapper.Map<EnderecoResponse>(enderecoCadastrado);
        }

        public async Task<EnderecoResponse> Put(EnderecoRequest enderecoRequest, int? id)
        {
            var enderecoBancoDeDados = await _enderecoRepository.GetById((int)id);

            var enderecoCadastrado = await _enderecoRepository.Put(enderecoBancoDeDados, null);

            return _mapper.Map<EnderecoResponse>(enderecoCadastrado);
        }
        public async Task Delete(int id)
        {
            await _enderecoRepository.Delete(id);
        }

       
    }
}
