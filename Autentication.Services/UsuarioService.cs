﻿using Autentication.Domain.Contracts.Enum;
using Autentication.Domain.Contracts.Usuario;
using Autentication.Domain.Entities;
using Autentication.Domain.Interfaces.Repository;
using Autentication.Domain.Interfaces.Service;
using Autentication.Domain.Shared;
using AutoMapper;

namespace Autentication.Services
{
    public class UsuarioService : IUsuarioService
    {
        public readonly IUsuarioRepository _usuarioRepository;
        public readonly IMapper _mapper;
        public UsuarioService(IUsuarioRepository usuarioRepository, IMapper mapper)
        {
            _usuarioRepository = usuarioRepository;
            _mapper = mapper;
        }

        public async Task<IEnumerable<UsuarioResponse>> Get()
        {
            throw new Exception("Recurso Indisponível");
        }

        public async Task<IEnumerable<UsuarioResponse>> Get(string role)
        {

            var listaUsuarioRetornoBaseDeDados = new List<UsuarioEntities>();
            var listaUsuarioRetornoBaseDados =  await _usuarioRepository.Get();
            //var enderecoUsuario = await _enderecoService.GetById();

            if (role == "Cliente")
            {
                foreach (var entity in listaUsuarioRetornoBaseDados)
                {
                    entity.Cpf = "***.***.***-**";
                }
            }

            return _mapper.Map<IEnumerable<UsuarioResponse>>(listaUsuarioRetornoBaseDados);
        }

        public async Task<UsuarioResponse> GetById(int id)
        {
            try
            {
                var ListaUsuarioRetornoBaseDados = await _usuarioRepository.GetById(id);

            if (ListaUsuarioRetornoBaseDados == null)
            {
                throw new Exception("Recurso Indisponível");
            }

            return _mapper.Map<UsuarioResponse>(ListaUsuarioRetornoBaseDados);
        }
            catch(Exception ex)
            {
                throw new Exception("Erro não tratado");
    }
}

        public async Task<UsuarioResponse> Post(UsuarioCadastraRequest usuarioRequest)
        {
            var requestUsuarioEntities = _mapper.Map<UsuarioEntities>(usuarioRequest);

            requestUsuarioEntities.Senha = Cryptography.Encrypt(usuarioRequest.Senha);

            var usuarioCadastrado = await _usuarioRepository.Post(requestUsuarioEntities);

            return _mapper.Map<UsuarioResponse>(usuarioCadastrado);
        }

        public Task<UsuarioResponse> Post(UsuarioRequest request)
        {
            throw new Exception("Recurso Indisponível");
        }

        public async Task<UsuarioResponse> Put(UsuarioRequest usuarioRequest, int? id)
        {
            var usuarioBancoDeDados = await _usuarioRepository.GetById((int)id);

            var usuarioCadastrado = await _usuarioRepository.Put(usuarioBancoDeDados, null);

            return _mapper.Map<UsuarioResponse>(usuarioCadastrado);
        }

        public async Task Delete(int id)
        {
             await _usuarioRepository.Delete(id);

        }
    }
}