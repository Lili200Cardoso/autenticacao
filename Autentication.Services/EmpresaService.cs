﻿using Autentication.Domain.Contracts.Empresa;
using Autentication.Domain.Entities;
using Autentication.Domain.Interfaces.Repository;
using Autentication.Domain.Interfaces.Service;
using AutoMapper;

namespace Autentication.Services
{
    public class EmpresaService : IEmpresaService
    {
        public readonly IEmpresaRepository _empresaRepository;
        public readonly IMapper _mapper;
        public EmpresaService(IEmpresaRepository empresaRepository, IMapper mapper)
        {
            _empresaRepository = empresaRepository;
            _mapper = mapper;
        }

        public async Task<IEnumerable<EmpresaResponse>> Get()
        {
            var ListaEmpresaRetornoBaseDados = await _empresaRepository.Get();

            return _mapper.Map<IEnumerable<EmpresaResponse>>(ListaEmpresaRetornoBaseDados);
        }

        public async Task<EmpresaResponse> GetById(int id)
        {
            var empresaRetornoBaseDados = await _empresaRepository.GetById(id);

            return _mapper.Map<EmpresaResponse>(empresaRetornoBaseDados);
        }

        public async Task<EmpresaResponse> Post(EmpresaRequest empresaRequest)
        {
            var requestEmpresaEntities = _mapper.Map<EmpresaEntities>(empresaRequest);

            var empresaCadastrada = await _empresaRepository.Post(requestEmpresaEntities);

            return _mapper.Map<EmpresaResponse>(empresaCadastrada);
        }

        public async Task<EmpresaResponse> Put(EmpresaRequest empresaRequest, int? id)
        {
            var empresaBancoDeDados = await _empresaRepository.GetById((int)id);

            var empresaCadastrado = await _empresaRepository.Put(empresaBancoDeDados, null);

            return _mapper.Map<EmpresaResponse>(empresaCadastrado);
        }
        public async Task Delete(int id)
        {
            await _empresaRepository.Delete(id);
        }

    }
}
