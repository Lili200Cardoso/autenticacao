﻿using Autentication.CrossCutting.DependencyInjection;
using Microsoft.Extensions.DependencyInjection;

namespace Autentication.IoC
{
    public static class NativeInjectorBootStrapper
    {
        public static void RegisterAppDependencies(this IServiceCollection services)
        {
            ConfigureService.ConfigureDependenciesService(services);
            ConfigureMapper.ConfigureDependenciesMapper(services);
        }

        public static void RegisterAppDependenciesContext(this IServiceCollection services, string connectionString)
        {
            ConfigureRepository.ConfigureDependenciesRepository(services, connectionString);
        }
    }
}