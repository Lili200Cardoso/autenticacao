﻿using Autentication.Domain.Entities;
using Autentication.Repository.Mappings;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Autentication.Repository
{
    public class AutenticationContext : DbContext
    {
        public DbSet<UsuarioEntities> Usuarios { get; set; }
        public DbSet<EmpresaEntities> Empresas { get; set; }
        public DbSet<EnderecoEntities> Enderecos { get; set; }

        public AutenticationContext(DbContextOptions<AutenticationContext> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<UsuarioEntities>(new UsuarioEntitiesMap().Configure);
            modelBuilder.Entity<EmpresaEntities>(new EmpresaEntitiesMap().Configure);
            modelBuilder.Entity<EnderecoEntities>(new EnderecoEntitiesMap().Configure);
        }
    }
}
