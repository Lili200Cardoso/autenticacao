﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Autentication.Repository.Migrations
{
    public partial class AdicionandoRolesMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Perfil",
                table: "Usuarios",
                newName: "Roles");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Roles",
                table: "Usuarios",
                newName: "Perfil");
        }
    }
}
