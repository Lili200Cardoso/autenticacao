﻿using Autentication.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Autentication.Repository.Mappings
{
    public class UsuarioEntitiesMap : IEntityTypeConfiguration<UsuarioEntities>
    {
        public void Configure(EntityTypeBuilder<UsuarioEntities> builder)
        {
            builder.ToTable("Usuarios");

            //Definindo uma propriedade como OPCIONAL.
            //builder.Property(x => x.Telefone).IsRequired(false);
        }
    }
}
