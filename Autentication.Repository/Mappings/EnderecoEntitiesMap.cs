﻿using Autentication.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Autentication.Repository.Mappings
{
    public class EnderecoEntitiesMap : IEntityTypeConfiguration<EnderecoEntities>
    {
        public void Configure(EntityTypeBuilder<EnderecoEntities> builder)
        {
            builder.ToTable("Usuarios");
        }
    }
}
