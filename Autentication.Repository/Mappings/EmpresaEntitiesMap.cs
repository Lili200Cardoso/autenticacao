﻿using Autentication.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Autentication.Repository.Mappings
{
    public class EmpresaEntitiesMap : IEntityTypeConfiguration<EmpresaEntities>
    {
        public void Configure(EntityTypeBuilder<EmpresaEntities> builder)
        {
            builder.ToTable("Usuarios");
        }
    }
}
