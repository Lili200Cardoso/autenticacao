﻿using Autentication.Domain.Contracts.Enum;

namespace Autentication.Domain.Entities
{
    public class UsuarioEntities
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public string Telefone { get; set; }
        public string? Email { get; set; }
        public string Senha { get; set; }
        public string Cpf { get; set; }
        public DateTime DataNascimento { get; set; }
        public EnderecoEntities Endereco { get; set; }
        public int EnderecoId { get; set; }
        public PerfilUsuarioEnum Roles { get; set; }
    }
}
