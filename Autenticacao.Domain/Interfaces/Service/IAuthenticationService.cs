﻿namespace Autentication.Domain.Interfaces.Service
{
    public interface IAuthenticationService
    {
        Task<string> Login(string email, string senha);

    }
}
