﻿using Autentication.Domain.Contracts.Empresa;

namespace Autentication.Domain.Interfaces.Service
{
    public interface IEmpresaService : IBaseCRUD<EmpresaRequest, EmpresaResponse> { }
}
