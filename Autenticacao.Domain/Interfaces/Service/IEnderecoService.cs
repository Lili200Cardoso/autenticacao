﻿using Autentication.Domain.Contracts.Endereco;

namespace Autentication.Domain.Interfaces.Service
{
    public interface IEnderecoService : IBaseCRUD<EnderecoRequest, EnderecoResponse> { }
}
