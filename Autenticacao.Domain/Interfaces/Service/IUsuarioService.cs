﻿using Autentication.Domain.Contracts.Usuario;

namespace Autentication.Domain.Interfaces.Service
{
    public interface IUsuarioService : IBaseCRUD<UsuarioRequest, UsuarioResponse>
    {
        Task<UsuarioResponse> Post(UsuarioCadastraRequest request);
        Task<IEnumerable<UsuarioResponse>> Get(string role);
    }
}
