﻿using Autentication.Domain.Entities;

namespace Autentication.Domain.Interfaces.Repository
{
    public interface IUsuarioRepository : IBaseCRUD<UsuarioEntities, UsuarioEntities>
    {
        Task Delete(UsuarioEntities request);
        Task<UsuarioEntities> GetByEmail(string? email);
    }

}
