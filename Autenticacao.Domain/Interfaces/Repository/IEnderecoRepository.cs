﻿using Autentication.Domain.Entities;

namespace Autentication.Domain.Interfaces.Repository
{
    public interface IEnderecoRepository : IBaseCRUD<EnderecoEntities, EnderecoEntities>
    {
        Task Delete(EnderecoEntities request);
    }
}
