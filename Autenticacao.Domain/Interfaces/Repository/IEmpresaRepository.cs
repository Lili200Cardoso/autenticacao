﻿using Autentication.Domain.Entities;

namespace Autentication.Domain.Interfaces.Repository
{
    public interface IEmpresaRepository : IBaseCRUD<EmpresaEntities, EmpresaEntities>
    {
        Task Delete(EmpresaEntities request);
    }
}
