﻿using Autentication.Domain.Contracts.Endereco;
using Autentication.Domain.Contracts.Enum;

namespace Autentication.Domain.Contracts.Usuario
{
    public class UsuarioResponse : UsuarioRequest
    {
        public string Nome { get; set; }
        public string Telefone { get; set; }
        public string Cpf { get; set; }
        public string? Email { get; set; }
        public DateTime DataNascimento { get; set; }
        public EnderecoResponse Endereco { get; set; }
        public PerfilUsuarioEnum Roles { get; set; }

      
    }
}
