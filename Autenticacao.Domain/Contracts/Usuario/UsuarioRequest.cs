using Autentication.Domain.Contracts.Endereco;
using Autentication.Domain.Contracts.Enum;

namespace Autentication
{
    public class UsuarioRequest
    {
        public int Id { get; set; }
        public string Nome { get; set; }
    } 
}
