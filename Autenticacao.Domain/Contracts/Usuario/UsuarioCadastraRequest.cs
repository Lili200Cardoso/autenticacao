﻿namespace Autentication.Domain.Contracts.Usuario
{
    public class UsuarioCadastraRequest : UsuarioRequest
    {
        public string? Senha { get; set; }
        public string? Email { get; set; }
        public string Nome { get; set; }
    }
}
