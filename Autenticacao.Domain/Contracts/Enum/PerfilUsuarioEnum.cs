﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Autentication.Domain.Contracts.Enum
{
    public enum  PerfilUsuarioEnum
    {
        Administrador,
        Cliente
    }
}
