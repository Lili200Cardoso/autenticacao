﻿using Autentication.Domain.Contracts.Endereco;

namespace Autentication.Domain.Contracts.Empresa
{
    public class EmpresaResponse : EmpresaRequest
    {
        public string Nome { get; set; }
        public string NomeFantasia { get; set; }
        public EnderecoResponse Endereco { get; set; }
    }
}


