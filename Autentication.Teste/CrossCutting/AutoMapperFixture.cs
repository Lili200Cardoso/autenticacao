﻿using Autentication.CrossCutting.Mappers;
using AutoMapper;
using System;

namespace Autentication.Teste.CrossCutting
{
    public abstract class BaseAutoMapperFixture
    {
        public IMapper mapper { get; set; }

        public BaseAutoMapperFixture()
        {
            mapper = new AutoMapperFixture().GetMapper();
        }

    }

    public class AutoMapperFixture : IDisposable
    {
        public IMapper GetMapper()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new UsuarioEntitiesToContractMap());
                cfg.AddProfile(new EmpresaEntitiesToContractMap());
                cfg.AddProfile(new EnderecoEntitiesToContractMap());
            });

            return config.CreateMapper();
        }

        public void Dispose() { }
    }
}
