﻿using Autentication.Domain.Entities;
using Autentication.Domain.Interfaces.Repository;
using Autentication.Services;
using Autentication.Teste.CrossCutting;
using Autentication.Teste.Fakers;
using AutoMapper;
using Moq;
using Xunit;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Autentication.Teste.Services
{
    public class UsuarioServiceTeste
    {
        private readonly Mock<IUsuarioRepository> _mockUsuarioRepository = new Mock<IUsuarioRepository>();
        public IMapper mapper = new AutoMapperFixture().GetMapper();

        [Fact(DisplayName = "Lista todos os Usuarios")]
        public async Task Get()
        {
            _mockUsuarioRepository.Setup(mock => mock.Get()).Returns(UsuarioEntitiesFaker.UsuarioEntitiesAsync());

            var service = new UsuarioService(_mockUsuarioRepository.Object, mapper);

            var result = await service.Get();

            Assert.True(result.ToList().Count() > 0);
        }


        [Fact(DisplayName = "Busca um usuario por ID")]
        public async Task GetById()
        {
            int id = UsuarioEntitiesFaker.GetId();

            _mockUsuarioRepository.Setup(mock => mock.GetById(id)).Returns(UsuarioEntitiesFaker.UsuarioEntitiesAsync(id));

            var service = new UsuarioService(_mockUsuarioRepository.Object, mapper);

            var result = await service.GetById(id);

            Assert.Equal(result.Id, id);
        }

        [Fact(DisplayName = "Cadastra um novo usuario")]
        public async Task Post()
        {
            var userRequest = UsuarioContractFaker.UsuarioRequest();
            var userRequestEntities = UsuarioEntitiesFaker.UsuarioEntities();
            var resultUserRequest = UsuarioEntitiesFaker.UsuarioEntitiesBaseRequestAsync(userRequestEntities.Nome);

            _mockUsuarioRepository.Setup(mock => mock.Post(It.IsAny<UsuarioEntities>())).Returns(resultUserRequest);

            var service = new UsuarioService(_mockUsuarioRepository.Object, mapper);

            var result = await service.Post(userRequest);

            Assert.Equal(result.Nome, resultUserRequest.Result.Nome);
        }

        [Fact(DisplayName = "Edita um usuario já existente")]
        public async Task Put()
        {
            var userRequest = UsuarioContractFaker.UsuarioRequest();
            var userRequestEntities = UsuarioEntitiesFaker.UsuarioEntities();
            var resultUserRequest = UsuarioEntitiesFaker.UsuarioEntitiesBaseRequestAsync(userRequestEntities.Nome);

            _mockUsuarioRepository.Setup(mock => mock.GetById(It.IsAny<int>())).Returns(resultUserRequest);
            _mockUsuarioRepository.Setup(mock => mock.Put(It.IsAny<UsuarioEntities>(), It.IsAny<int?>())).Returns(resultUserRequest);

            var service = new UsuarioService(_mockUsuarioRepository.Object, mapper);

            var result = await service.Put(userRequest, resultUserRequest.Result.Id);

            Assert.Equal(result.Nome, resultUserRequest.Result.Nome);
        }

        [Fact(DisplayName = "Remove um usuario já existente")]
        public async Task Delete()
        {
            int id = UsuarioEntitiesFaker.GetId();

            _mockUsuarioRepository.Setup(mock => mock.Delete(id)).Returns(() => Task.FromResult(string.Empty));

            var service = new UsuarioService(_mockUsuarioRepository.Object, mapper);

            try
            {
                await service.Delete(id);
            }
            catch (System.Exception)
            {
                Assert.True(false);
            }
        }
    }

}
