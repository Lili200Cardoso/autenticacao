﻿using Autentication.Domain.Entities;
using Autentication.Domain.Interfaces.Repository;
using Autentication.Services;
using Autentication.Teste.CrossCutting;
using Autentication.Teste.Fakers;
using AutoMapper;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Autentication.Teste.Services
{
    public class EmpresaServiceTeste
    {
        private readonly Mock<IEmpresaRepository> _mockEmpresaRepository = new Mock<IEmpresaRepository>();
        public IMapper mapper = new AutoMapperFixture().GetMapper();

        [Fact(DisplayName = "Lista todas as Empresas")]
        public async Task Get()
        {
            _mockEmpresaRepository.Setup(mock => mock.Get()).Returns(EmpresaEntitiesFaker.EmpresaEntitiesAsync());

            var service = new EmpresaService(_mockEmpresaRepository.Object, mapper);

            var result = await service.Get();

            Assert.True(result.ToList().Count() > 0);
        }


        [Fact(DisplayName = "Busca uma Empresa por ID")]
        public async Task GetById()
        {
            int id = EmpresaEntitiesFaker.GetId();

            _mockEmpresaRepository.Setup(mock => mock.GetById(id)).Returns(EmpresaEntitiesFaker.EmpresaEntitiesAsync(id));

            var service = new EmpresaService(_mockEmpresaRepository.Object, mapper);

            var result = await service.GetById(id);

            Assert.Equal(result.Id, id);
        }

        [Fact(DisplayName = "Cadastra uma nova Empresa")]
        public async Task Post()
        {
            var companyRequest = EmpresaContractFaker.EmpresaRequest();
            var companyRequestEntities = EmpresaEntitiesFaker.EmpresaEntities();
            var resultCompanyRequest = EmpresaEntitiesFaker.EmpresaEntitiesBaseRequestAsync(companyRequestEntities.Nome);

            _mockEmpresaRepository.Setup(mock => mock.Post(It.IsAny<EmpresaEntities>())).Returns(resultCompanyRequest);

            var service = new EmpresaService(_mockEmpresaRepository.Object, mapper);

            var result = await service.Post(companyRequest);

            Assert.Equal(result.Nome, resultCompanyRequest.Result.Nome);
        }

        [Fact(DisplayName = "Edita uma Empresa já existente")]
        public async Task Put()
        {
            var companyRequest = EmpresaContractFaker.EmpresaRequest();
            var companyRequestEntities = EmpresaEntitiesFaker.EmpresaEntities();
            var resultCompanyRequest = EmpresaEntitiesFaker.EmpresaEntitiesBaseRequestAsync(companyRequestEntities.Nome);

            _mockEmpresaRepository.Setup(mock => mock.GetById(It.IsAny<int>())).Returns(resultCompanyRequest);
            _mockEmpresaRepository.Setup(mock => mock.Put(It.IsAny<EmpresaEntities>(), It.IsAny<int?>())).Returns(resultCompanyRequest);

            var service = new EmpresaService(_mockEmpresaRepository.Object, mapper);

            var result = await service.Put(companyRequest, resultCompanyRequest.Result.Id);

            Assert.Equal(result.Nome, resultCompanyRequest.Result.Nome);
        }

        [Fact(DisplayName = "Remove uma Empresa já existente")]
        public async Task Delete()
        {
            int id = EmpresaEntitiesFaker.GetId();

            _mockEmpresaRepository.Setup(mock => mock.Delete(id)).Returns(() => Task.FromResult(string.Empty));

            var service = new EmpresaService(_mockEmpresaRepository.Object, mapper);

            try
            {
                await service.Delete(id);
            }
            catch (System.Exception)
            {
                Assert.True(false);
            }
        }
    }
}
