﻿using Autentication.Domain.Entities;
using Bogus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Autentication.Teste.Fakers
{
    public class EnderecoEntitiesFaker
    {
        private static readonly Faker Fake = new Faker();

        public EnderecoEntities CriandoEnderecoEntitiesAsync()
        {
            var meuEndereco = new EnderecoEntities();

            meuEndereco = new EnderecoEntities()
            {
                Id = Fake.IndexFaker,
                Rua = Fake.Address.StreetName(),
                Numero = Fake.IndexFaker,
                Bairro = Fake.Address.StreetName(),
                Cidade = Fake.Address.City(),
                Estado = Fake.Address.Country(),
                Cep = Fake.Address.ZipCode()

            };

            return meuEndereco;
        }
    }
}
