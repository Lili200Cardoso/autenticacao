﻿using Autentication.Domain.Contracts.Empresa;
using Bogus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Autentication.Teste.Fakers
{
    public static class EmpresaContractFaker
    {
        private static readonly Faker Fake = new Faker();

        public static int GetId()
        {
            return Fake.IndexFaker;
        }

        public static async Task<IEnumerable<EmpresaResponse>> EmpresaResponseAsync()
        {
            var minhaLista = new List<EmpresaResponse>();

            for (int i = 0; i < 5; i++)
            {
                //var endereco = EnderecoContractFaker.CriandoEnderecoAsync()
                minhaLista.Add(new EmpresaResponse()
                {
                    Id = i,
                    Nome = Fake.Name.FirstName(),
                    NomeFantasia = Fake.Name.FirstName(),
                    //Endereco =

                });
            }

            return minhaLista;
        }

        public static async Task<EmpresaResponse> EmpresaResponseAsync(int id)
        {
            return new EmpresaResponse()
            {
                Id = id,
                Nome = Fake.Name.FirstName()
            };
        }

        public static EmpresaRequest EmpresaRequest()
        {
            return new EmpresaRequest
            {
                Nome = Fake.Name.FirstName()
            };
        }

        public static EmpresaRequest EmpresaCadastraRequest()
        {
            return new EmpresaRequest
            {
                Nome = Fake.Name.FirstName(),

            };
        }

        public static async Task<EmpresaResponse> EmpresaResponseBaseRequestAsync(string nome)
        {
            return new EmpresaResponse()
            {
                Id = Fake.IndexFaker,
                Nome = nome
            };
        }

    }
  
}
