﻿using Autentication.Domain.Entities;
using Bogus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Autentication.Teste.Fakers
{
    public static class EmpresaEntitiesFaker
    {
        private static readonly Faker Fake = new Faker();

        public static int GetId()
        {
            return Fake.IndexFaker;
        }

        public static async Task<IEnumerable<EmpresaEntities>> EmpresaEntitiesAsync()
        {
            var minhaLista = new List<EmpresaEntities>();

            for (int i = 0; i < 5; i++)
            {
                minhaLista.Add(new EmpresaEntities()
                {
                    Id = i,
                    Nome = Fake.Name.FirstName(),
                    NomeFantasia = Fake.Name.FirstName(),
                    //Endereco =
                });
            }

            return minhaLista;
        }

        public static async Task<EmpresaEntities> EmpresaEntitiesAsync(int id)
        {
            return new EmpresaEntities()
            {
                Id = id,
                Nome = Fake.Name.FirstName()
            };
        }

        public static EmpresaEntities EmpresaEntities()
        {
            return new EmpresaEntities
            {
                Nome = Fake.Name.FirstName()
            };
        }

        public static async Task<EmpresaEntities> EmpresaEntitiesBaseRequestAsync(string nome)
        {
            return new EmpresaEntities()
            {
                Id = Fake.IndexFaker,
                Nome = nome
            };
        }
    }
}
