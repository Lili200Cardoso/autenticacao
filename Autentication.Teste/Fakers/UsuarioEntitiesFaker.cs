﻿using Autentication.Domain.Entities;
using Autentication.Domain.Contracts.Usuario;
using Bogus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autentication.Domain.Contracts.Enum;

namespace Autentication.Teste.Fakers
{
    public static class UsuarioEntitiesFaker
    {
        private static readonly Faker Fake = new Faker("pt_BR");

        public static int GetId()
        {
            return Fake.IndexFaker;
        }

        public static async Task<IEnumerable<UsuarioEntities>> UsuarioEntitiesAsync()
        {

            var usuario = new Faker<UsuarioEntities>()
           .RuleFor(u => u.Id, u => u.IndexFaker)
           .RuleFor(u => u.Nome, u => u.Name.FirstName())
           .RuleFor(u => u.Telefone, u => u.Phone.PhoneNumber())
           .RuleFor(u => u.Email, u => u.Internet.DomainWord())
           .RuleFor(u => u.Cpf, u => u.Random.Int().ToString())
           .RuleFor(u => u.DataNascimento, u => u.Date.Past(15))
           .RuleFor(u => u.Endereco, u => new EnderecoEntitiesFaker().CriandoEnderecoEntitiesAsync())
           .RuleFor(u => u.Roles, u => u.PickRandom<PerfilUsuarioEnum>());
           //.Generate();


            return (IEnumerable<UsuarioEntities>)usuario;


        }

        public static async Task<UsuarioEntities> UsuarioEntitiesAsync(int id)
        {
            return new UsuarioEntities()
            {
                Id = id,
                Nome = Fake.Name.FirstName()
            };
        }

        public static UsuarioEntities UsuarioEntities()
        {
            return new UsuarioEntities
            {
                Nome = Fake.Name.FirstName()
            };
        }

        public static async Task<UsuarioEntities> UsuarioEntitiesBaseRequestAsync(string nome)
        {
            return new UsuarioEntities()
            {
                Id = Fake.IndexFaker,
                Nome = nome
            };
        }
    }
}
