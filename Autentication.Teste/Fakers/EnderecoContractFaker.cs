﻿using Autentication.Domain.Contracts.Endereco;
using Bogus;

namespace Autentication.Teste.Fakers
{
    public class EnderecoContractFaker
    {
        private static readonly Faker Fake = new Faker();

        public EnderecoResponse CriandoEnderecoAsync()
        {
            var meuEndereco = new EnderecoResponse();

            meuEndereco = new EnderecoResponse()
            {
                Id = Fake.IndexFaker,
                Rua = Fake.Address.StreetName(),
                Numero = Fake.IndexFaker,
                Bairro = Fake.Address.StreetName(),
                Cidade = Fake.Address.City(),
                Estado = Fake.Address.Country(),
                Cep = Fake.Address.ZipCode()

            };

            return meuEndereco;
        }
    }
}
