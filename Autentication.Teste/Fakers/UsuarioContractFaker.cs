﻿using Autentication.Domain.Contracts.Usuario;
using Autentication.Domain.Contracts.Endereco;
using Bogus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autentication.Domain.Contracts.Enum;

namespace Autentication.Teste.Fakers
{
    public static class UsuarioContractFaker
    {
        private static readonly Faker Fake = new Faker("pt_BR");

        public static int GetId()
        {
            return Fake.IndexFaker;
        }

        public static async Task<IEnumerable<UsuarioResponse>> UsuarioResponseAsync()
        {


            var usuario = new Faker<UsuarioResponse>()
           .RuleFor(u => u.Id, u => u.IndexFaker)
           .RuleFor(u => u.Nome, u => u.Name.FirstName())
           .RuleFor(u => u.Telefone, u => u.Phone.PhoneNumber())
           .RuleFor(u => u.Email, u => u.Internet.DomainWord())
           .RuleFor(u => u.Cpf, u => u.Random.Int().ToString())
           .RuleFor(u => u.DataNascimento, u => u.Date.Past(15))
           .RuleFor(u => u.Endereco, u => new EnderecoContractFaker().CriandoEnderecoAsync())
           .RuleFor(u => u.Roles, u => u.PickRandom<PerfilUsuarioEnum>());
                //.Generate();


             return (IEnumerable<UsuarioResponse>)usuario;


        }  
        
        public static async Task<UsuarioResponse> UsuarioResponseAsync(int id)
        {
            return new UsuarioResponse()
            {
                Id = id,
                Nome = Fake.Name.FirstName()
            };
        }

        public static UsuarioRequest UsuarioRequest()
        {
            return new UsuarioRequest
            {
                Nome = Fake.Name.FirstName()
            };
        }

        public static UsuarioCadastraRequest UsuarioLoginRequest()
        {
            return new UsuarioCadastraRequest
            {
                Email = "bebel@gmail.com", // Fake.Internet.ToString(),
                Senha = "bebel"
            };
        }



        public static UsuarioCadastraRequest UsuarioCadastraRequest()
        {
            return new UsuarioCadastraRequest
            {
                Nome = Fake.Name.FirstName(),
                Senha = Fake.Name.FullName()
            };
        }

        public static async Task<UsuarioResponse> UsuarioResponseBaseRequestAsync(string nome)
        {
            return new UsuarioResponse()
            {
                Id = Fake.IndexFaker,
                Nome = nome,
                
            };
        }
    }

}
