﻿using Autentication.Controllers;
using Autentication.Domain.Interfaces.Service;
using Autentication.Teste.Fakers;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Autentication.Teste.Controllers
{
    [Trait("Controller", "Controller de Empresas")]
    public class EmpresaControllerTeste
    {

        private readonly Mock<IEmpresaService> _mockEmpresaService = new Mock<IEmpresaService>();


        [Fact(DisplayName = "Lista todas as Empresa")]
        public async Task Get()
        {
            _mockEmpresaService.Setup(mock => mock.Get()).Returns(EmpresaContractFaker.EmpresaResponseAsync());

            var controller = new EmpresaController(_mockEmpresaService.Object);

            var result = await controller.Get();

            Assert.True(result.ToList().Count() > 0);
        }

        [Fact(DisplayName = "Busca uma Empresa por ID")]
        public async Task GetById()
        {
            int id = EmpresaContractFaker.GetId();

            _mockEmpresaService.Setup(mock => mock.GetById(id)).Returns(EmpresaContractFaker.EmpresaResponseAsync(id));

            var controller = new EmpresaController(_mockEmpresaService.Object);

            var result = await controller.GetById(id);

            Assert.Equal(result.Id, id);
        }

        [Fact(DisplayName = "Cadastra uma nova Empresa")]
        public async Task Post()
        {
            var companyRequest = EmpresaContractFaker.EmpresaRequest();
            var resultCompanyRequest = EmpresaContractFaker.EmpresaResponseBaseRequestAsync(companyRequest.Nome);

            _mockEmpresaService.Setup(mock => mock.Post(companyRequest)).Returns(resultCompanyRequest);

            var controller = new EmpresaController(_mockEmpresaService.Object);

            var result = await controller.Post(companyRequest);

            Assert.Equal(result.Nome, resultCompanyRequest.Result.Nome);
        }

        [Fact(DisplayName = "Edita uma Empresa já existente")]
        public async Task Put()
        {
            var companyRequest = EmpresaContractFaker.EmpresaRequest();
            var resultCompanyRequest = EmpresaContractFaker.EmpresaResponseBaseRequestAsync(companyRequest.Nome);

            _mockEmpresaService.Setup(mock => mock.Put(companyRequest, resultCompanyRequest.Result.Id)).Returns(resultCompanyRequest);

            var controller = new EmpresaController(_mockEmpresaService.Object);

            var result = await controller.Put(resultCompanyRequest.Result.Id, companyRequest);

            Assert.Equal(result.Nome, resultCompanyRequest.Result.Nome);
        }

        [Fact(DisplayName = "Remove uma Empresa já existente")]
        public async Task Delete()
        {
            int id = EmpresaContractFaker.GetId();

            _mockEmpresaService.Setup(mock => mock.Delete(id)).Returns(() => Task.FromResult(string.Empty));

            var controller = new EmpresaController(_mockEmpresaService.Object);

            try
            {
                await controller.Delete(id);
            }
            catch (System.Exception)
            {
                Assert.True(false);
            }


        }
    }
}
