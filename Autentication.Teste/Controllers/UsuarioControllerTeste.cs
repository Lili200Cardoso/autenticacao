﻿using Autentication.Controllers;
using Autentication.Domain.Interfaces.Service;
using Autentication.Teste.Fakers;
using System.Linq;
using System.Threading.Tasks;
using Moq;
using Xunit;
using Autentication.Services;
using Microsoft.Extensions.DependencyInjection;
using Autentication.Domain.Interfaces.Repository;
using Autentication.Repository;

namespace Autentication.Teste.Controllers
{
    [Trait("Controller", "Controller de Usuários")]
    public class UsuarioControllerTeste
    {
         private readonly Mock<IUsuarioService> _mockUsuarioService = new Mock<IUsuarioService>();
        //private readonly IUsuarioRepository _usuarioRepository;
        private IAuthenticationService _authenticationService;        


        [Fact(DisplayName = "Lista todos os Usuarios")]
        public async Task Get()
        {
            var autenticacaoController = new AuthenticationController(_authenticationService);
            var autenticacao = autenticacaoController.Login("bebel@gmail.com", "bebel");

            _mockUsuarioService.Setup(mock => mock.Get()).Returns(UsuarioContractFaker.UsuarioResponseAsync());

            var controller = new UsuarioController(_mockUsuarioService.Object);

            var result = await controller.Get();


            Assert.True(result.ToList().Count() > 0);

            Assert.NotNull(autenticacao);


            //_mockUsuarioService.Setup(mock => mock.Get()).Returns(UsuarioContractFaker.UsuarioResponseAsync());

            //var controller = new UsuarioController(_mockUsuarioService.Object);

            //var result = await controller.Get();

            //Assert.True(result.ToList().Count() > 0);
        }

        [Fact(DisplayName = "Busca um usuario por ID")]
        public async Task GetById()
        {
            int id = UsuarioContractFaker.GetId();
            _mockUsuarioService.Setup(mock => mock.GetById(id)).Returns(UsuarioContractFaker.UsuarioResponseAsync(id));
            var controller = new UsuarioController(_mockUsuarioService.Object);
            

            var result = await controller.GetById(id);

            Assert.Equal(result.Id, id);
        }

        [Fact(DisplayName = "Cadastra um novo usuario")]
        public async Task Post()
        {
            var userRequest = UsuarioContractFaker.UsuarioCadastraRequest();
            var resultUserRequest = UsuarioContractFaker.UsuarioResponseBaseRequestAsync(userRequest.Nome);

            _mockUsuarioService.Setup(mock => mock.Post(userRequest)).Returns(resultUserRequest);

            var controller = new UsuarioController(_mockUsuarioService.Object);

            var result = await controller.Post(userRequest);

            Assert.Equal(result.Nome, resultUserRequest.Result.Nome);
        }

        [Fact(DisplayName = "Edita um usuario já existente")]
        public async Task Put()
        {
            var userRequest = UsuarioContractFaker.UsuarioRequest();
            var resultUserRequest = UsuarioContractFaker.UsuarioResponseBaseRequestAsync(userRequest.Nome);

            _mockUsuarioService.Setup(mock => mock.Put(userRequest, resultUserRequest.Result.Id)).Returns(resultUserRequest);

            var controller = new UsuarioController(_mockUsuarioService.Object);

            var result = await controller.Put(resultUserRequest.Result.Id, userRequest);

            Assert.Equal(result.Nome, resultUserRequest.Result.Nome);
        }

        [Fact(DisplayName = "Remove um usuario já existente")]
        public async Task Delete()
        {
            int id = UsuarioContractFaker.GetId();

            _mockUsuarioService.Setup(mock => mock.Delete(id)).Returns(() => Task.FromResult(string.Empty));

            var controller = new UsuarioController(_mockUsuarioService.Object);

            try
            {
                await controller.Delete(id);
            }
            catch (System.Exception)
            {
                Assert.True(false);
            }


        }
    }
}
