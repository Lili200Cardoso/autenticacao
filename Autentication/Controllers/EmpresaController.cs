﻿using Autentication.Domain.Contracts.Empresa;
using Autentication.Domain.Interfaces.Service;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Autentication.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class EmpresaController : ControllerBase
    {
        public readonly IEmpresaService _empresaService;

        public EmpresaController(IEmpresaService empresaService)
        {
            _empresaService = empresaService;
        }

        /// <summary>
        /// Através dessa rota você será capaz de Buscar uma lista de Empresas.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize(Roles = "Administrador")]
        public async Task<IEnumerable<EmpresaResponse>> Get()
        {
            return await _empresaService.Get();
        }

        /// <summary>
        /// Através dessa rota você será capaz de Buscar uma Empresa pelo seu ID.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        [Authorize(Roles = "Administrador")]
        public async Task<EmpresaResponse> GetById(int id)
        {
            return await _empresaService.GetById(id);
        }

        /// <summary>
        /// Através dessa rota você será capaz de Cadastrar uma Empresa.
        /// </summary>
        /// <param name="empresaRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "Administrador")]
        public async Task<EmpresaResponse> Post(EmpresaRequest empresaRequest)
        {
            return await _empresaService.Post(empresaRequest);
        }

        /// <summary>
        /// Através dessa rota você será capaz de Editar uma Empresa.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="empresaRequest"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        [Authorize(Roles = "Administrador")]
        public async Task<EmpresaResponse> Put(int id, EmpresaRequest empresaRequest)
        {
            return await _empresaService.Put(empresaRequest, id);
        }

        /// <summary>
        /// Através dessa rota você será capaz de Deletar uma Empresa.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        [Authorize(Roles = "Administrador")]
        public async Task Delete(int id)
        {
            await _empresaService.Delete(id);

        }
    }
}

