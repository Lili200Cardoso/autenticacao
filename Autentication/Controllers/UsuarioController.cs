using Autentication.Domain.Contracts.Usuario;
using Autentication.Domain.Interfaces.Service;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

using System.Security.Claims;

namespace Autentication.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class UsuarioController : ControllerBase
    {
        public readonly IUsuarioService _usuarioService;

        //Respostas de informao (100-199),
        //Respostas de sucesso (200-299),
        //Redirecionamentos (300-399)
        //Erros do cliente (400-499)
        //Erros do servidor (500-599).


        public UsuarioController(IUsuarioService usuarioService)
        {
            _usuarioService = usuarioService;
        }

        /// <summary>
        /// Atrav�s dessa rota voc� ser� capaz de Buscar uma lista de Usu�rios.
        /// </summary>
        /// <returns></returns>
               
        [HttpGet]
        [Authorize(Roles = "Administrador, Cliente")]
        public async Task<IEnumerable<UsuarioResponse>> Get()
        {   //Buscando a role do Usu�rio
            var role = User.FindFirst(ClaimTypes.Role).Value;
            return await _usuarioService.Get(role);
        }

        /// <summary>
        /// Atrav�s dessa rota voc� ser� capaz de Buscar um Usu�rio.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        /// 
        [HttpGet("{id}")]
        [Authorize(Roles = "Administrador")]
        public async Task<UsuarioResponse> GetById(int id)
        {
            return await _usuarioService.GetById(id);
        }

        /// <summary>
        /// Atrav�s dessa rota voc� ser� capaz Cadastrar um Usu�rio.
        /// </summary>
        /// <param name="usuarioRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "Administrador")]
        public async Task<UsuarioResponse> Post(UsuarioCadastraRequest usuarioRequest)
        {
            return await _usuarioService.Post(usuarioRequest);
        }

        /// <summary>
        /// Atrav�s dessa rota voc� ser� capaz de Editar um Usu�rio.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="usuarioRequest"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        [Authorize(Roles = "Administrador")]
        public async Task<UsuarioResponse> Put(int id, UsuarioRequest usuarioRequest)
        {
            return await _usuarioService.Put(usuarioRequest, id);
        }

        /// <summary>
        /// Atrav�s dessa rota voc� ser� capaz de Deletar um Usu�rio.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        [Authorize(Roles = "Administrador")]
      
        public async Task Delete(int id)
        {
            await _usuarioService.Delete(id);

        }
    }
}