﻿using Autentication.Domain.Interfaces.Service;
using Microsoft.AspNetCore.Mvc;

namespace Autentication.Controllers
{
    [ApiController]
    [Route("[Controller]")]
    public class AuthenticationController : ControllerBase
    {
        private readonly IAuthenticationService _authenticationService;
        public AuthenticationController(IAuthenticationService authenticationService)
        {
            _authenticationService = authenticationService;
        }
        
        /// <summary>
        /// Através dessa rota um Usuário efetuara o seu Login.
        /// </summary>
        /// <param name="email"></param>
        /// <param name="senha"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<string>Login(string senha, string? email)
        {
                return await _authenticationService.Login(senha, email);
        }

    }
}
