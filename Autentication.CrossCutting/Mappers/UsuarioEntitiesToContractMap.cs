﻿using Autentication.Domain.Contracts.Usuario;
using Autentication.Domain.Entities;
using AutoMapper;

namespace Autentication.CrossCutting.Mappers
{
    public class UsuarioEntitiesToContractMap : Profile
    {
        public UsuarioEntitiesToContractMap()
        {
            CreateMap<UsuarioEntities, UsuarioRequest>().ReverseMap();
            CreateMap<UsuarioEntities, UsuarioCadastraRequest>().ReverseMap();
            CreateMap<UsuarioEntities, UsuarioResponse>().ReverseMap();
          
           
        }
    }
}
