﻿using Autentication.Domain.Contracts.Endereco;
using Autentication.Domain.Entities;
using AutoMapper;

namespace Autentication.CrossCutting.Mappers
{
    public class EnderecoEntitiesToContractMap : Profile
    {
        public EnderecoEntitiesToContractMap()
        {
            CreateMap<EnderecoEntities, EnderecoResponse>().ReverseMap();
            CreateMap<EnderecoEntities, EnderecoRequest>().ReverseMap();
        }
    }
}
