﻿using Autentication.Domain.Contracts.Empresa;
using Autentication.Domain.Entities;
using AutoMapper;

namespace Autentication.CrossCutting.Mappers
{
    public class EmpresaEntitiesToContractMap : Profile
    {
        public EmpresaEntitiesToContractMap()
        {
            CreateMap<EmpresaEntities, EmpresaResponse>().ReverseMap();
            CreateMap<EmpresaEntities, EmpresaRequest>().ReverseMap();

        }
    }
}
