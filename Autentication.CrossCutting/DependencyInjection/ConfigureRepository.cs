﻿using Autentication.Domain.Interfaces.Repository;
using Autentication.Repository;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;

namespace Autentication.CrossCutting.DependencyInjection
{
    public static class ConfigureRepository
    {
        public static void ConfigureDependenciesRepository(IServiceCollection serviceCollection, string connectionString)
        {
            serviceCollection.AddScoped<IUsuarioRepository, UsuarioRepository>();
            serviceCollection.AddScoped<IEmpresaRepository, EmpresaRepository>();
            serviceCollection.AddScoped<IEnderecoRepository, EnderecoRepository>();

            serviceCollection.AddDbContext<AutenticationContext>(options => options.UseSqlServer(connectionString));
        }
    }
}
