﻿using Autentication.Domain.Interfaces.Service;
using Autentication.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;

namespace Autentication.CrossCutting.DependencyInjection
{
    public static class ConfigureService
    {
        public static void ConfigureDependenciesService(IServiceCollection serviceCollection)
        {
            serviceCollection.AddScoped<IUsuarioService, UsuarioService>();
            serviceCollection.AddScoped<IEmpresaService, EmpresaService>();
            serviceCollection.AddScoped<IEnderecoService, EnderecoService>();
            serviceCollection.AddScoped<IAuthenticationService, AuthenticationService>();
            
        }
    }
}
