﻿using Autentication.CrossCutting.Mappers;
using AutoMapper;
using Microsoft.Extensions.DependencyInjection;

namespace Autentication.CrossCutting.DependencyInjection
{
    public static class ConfigureMapper
    {
        public static void ConfigureDependenciesMapper(IServiceCollection serviceCollection)
        {
            var config = new MapperConfiguration(cnf =>
            {
                cnf.AddProfile(new UsuarioEntitiesToContractMap());
                cnf.AddProfile(new EmpresaEntitiesToContractMap());
                cnf.AddProfile(   new EnderecoEntitiesToContractMap());
            });

            var mapConfiguration = config.CreateMapper();
            serviceCollection.AddSingleton(mapConfiguration);
        }
    }
}
